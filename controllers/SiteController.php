<?php

namespace app\controllers;

use app\models\Figure;
use app\models\TXTRenderStrategy;
use yii\web\Controller;

class SiteController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (\Yii::$app->request->isPost) {
            $figure = Figure::getInstance('square');
            $figure->setAttributes($_POST);
            $figure->setRenderStrategy(new TXTRenderStrategy());
            return $figure->render();
        }
        return $this->render('index');
    }

}
