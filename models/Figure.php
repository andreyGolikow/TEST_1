<?php
/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 22.11.17
 * Time: 13:40
 */

namespace app\models;


use yii\base\Model;
use \Error;
use \yii\base\Exception;
abstract class Figure extends Model
{
    /**
     * @var RenderStrategy
     */
    private $renderStrategy;

    public $height;

    public $width;

    public function render() : string
    {
        return $this->renderStrategy->render($this->getCoordinates());
    }

    /**
     * @param mixed $renderStrategy
     */
    public function setRenderStrategy(RenderStrategy $renderStrategy)
    {
        $this->renderStrategy = $renderStrategy;
    }

    /**
     * @return array
     */
    abstract protected function getCoordinates() : array;

    /**
     * @param $figureName
     * @return mixed
     * @throws \yii\base\Exception
     */
    public static function getInstance($figureName)
    {
        $figureName = ucfirst($figureName);
        $classNameSpace = 'app\models\\'.$figureName;
        try {
            return new $classNameSpace() ;
        } catch (Error $e) {
            throw new Exception('Не верный тип!');
        }
    }
}