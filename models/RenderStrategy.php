<?php
/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 22.11.17
 * Time: 13:44
 */

namespace app\models;


abstract class RenderStrategy
{
    /**
     * @param array $coordinates
     * @return string
     */
    abstract public function render(array $coordinates) : string;
}